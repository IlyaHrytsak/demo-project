package com.example.demo.service;

import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.TreeMap;

@Service
public class TransferTextServiceImpl implements TransferTextService {

    @Override
    public Map<String, Integer> transferText(String nativeText) {
        Map<String, Integer> sortedWords =
                new TreeMap<>((s1, s2) -> s2.toLowerCase().compareToIgnoreCase(s1.toLowerCase()));
        String[] unsortedWords = nativeText.split("\\W+");
        for (String word : unsortedWords) {
            if (!sortedWords.containsKey(word)) {
                sortedWords.put(word, 1);
            } else {
                Integer value = sortedWords.get(word);
                sortedWords.replace(word, ++value);
            }

        }
        return sortedWords;
    }

}
