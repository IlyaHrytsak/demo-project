package com.example.demo.service;

import java.util.Map;

public interface TransferTextService {

    Map<String, Integer> transferText(String nativeText);

}
