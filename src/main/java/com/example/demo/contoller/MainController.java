package com.example.demo.contoller;

import com.example.demo.dto.InputStringDto;
import com.example.demo.service.TransferTextService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequiredArgsConstructor
public class MainController {

    private final TransferTextService transferTextService;

    @PostMapping(value = "/transfer-text")
    public Map<String, Integer> transferText(@Valid @RequestBody InputStringDto inputStringDto) {
        String input = inputStringDto.getInput();
        return transferTextService.transferText(input);
    }

}
