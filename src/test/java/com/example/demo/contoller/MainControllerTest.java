package com.example.demo.contoller;

import com.example.demo.dto.InputStringDto;
import com.example.demo.service.TransferTextService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(MainController.class)
public class MainControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private TransferTextService transferTextService;
    private ObjectMapper objectMapper;

    @Before
    public void setUp() throws Exception {
        objectMapper = new ObjectMapper();
    }

    @Test
    public void inputStringWithOneWordTest() throws Exception {
        String inputString = "Hello";
        InputStringDto inputStringDto = new InputStringDto(inputString);
        Map<String, Integer> map = new HashMap<>();
        map.put("Hello", 1);
        when(transferTextService.transferText(inputString)).thenReturn(map);
        String json = objectMapper.writeValueAsString(inputStringDto);
        String expectedJson = objectMapper.writeValueAsString(map);
        mockMvc.perform(post("/transfer-text")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJson));
    }

    @Test
    public void inputStringWithOneDuplicateTest() throws Exception {
        String inputString = "Hello, Hello";
        InputStringDto inputStringDto = new InputStringDto(inputString);
        Map<String, Integer> map = new HashMap<>();
        map.put("Hello", 2);
        when(transferTextService.transferText(inputString)).thenReturn(map);
        String json = objectMapper.writeValueAsString(inputStringDto);
        String expectedJson = objectMapper.writeValueAsString(map);
        mockMvc.perform(post("/transfer-text")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJson));
    }

    @Test
    public void inputStringWithOneDuplicateAndTwoNormalWordsTest() throws Exception {
        String inputString = "Hello, Hello Man And";
        InputStringDto inputStringDto = new InputStringDto(inputString);
        Map<String, Integer> map = new HashMap<>();
        map.put("Hello", 2);
        map.put("Man", 1);
        map.put("And", 1);
        when(transferTextService.transferText(inputString)).thenReturn(map);
        String json = objectMapper.writeValueAsString(inputStringDto);
        String expectedJson = objectMapper.writeValueAsString(map);
        mockMvc.perform(post("/transfer-text")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJson));
    }

    @Test
    public void emptyInputStringTest() throws Exception {
        String inputString = "";
        InputStringDto inputStringDto = new InputStringDto(inputString);
        String json = objectMapper.writeValueAsString(inputStringDto);
        mockMvc.perform(post("/transfer-text")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void nullInputStringTest() throws Exception {
        InputStringDto inputStringDto = new InputStringDto();
        String json = objectMapper.writeValueAsString(inputStringDto);
        mockMvc.perform(post("/transfer-text")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isBadRequest());
    }

}